<?php

namespace eezeecommerce\PaymentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="eezeecommerce\PaymentBundle\Entity\PaymentSettingsRepository")
 */
class PaymentSettings
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sandbox;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="GatewayConfig", mappedBy="setting")
     */
    private $gateways;

    public function __construct()
    {
        $this->types = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sandbox
     *
     * @param boolean $sandbox
     *
     * @return PaymentSettings
     */
    public function setSandbox($sandbox)
    {
        $this->sandbox = $sandbox;

        return $this;
    }

    /**
     * Get sandbox
     *
     * @return boolean
     */
    public function getSandbox()
    {
        return $this->sandbox;
    }

    /**
     * Add gateway
     *
     * @param \eezeecommerce\PaymentBundle\Entity\GatewayConfig $gateway
     *
     * @return PaymentSettings
     */
    public function addGateway(\eezeecommerce\PaymentBundle\Entity\GatewayConfig $gateway)
    {
        $this->gateways[] = $gateway;

        $gateway->setSetting($this);

        return $this;
    }

    /**
     * Remove gateway
     *
     * @param \eezeecommerce\PaymentBundle\Entity\GatewayConfig $gateway
     */
    public function removeGateway(\eezeecommerce\PaymentBundle\Entity\GatewayConfig $gateway)
    {
        $this->gateways->removeElement($gateway);
    }

    /**
     * Get gateways
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGateways()
    {
        return $this->gateways;
    }
}
