<?php

namespace eezeecommerce\PaymentBundle\EventListener;


use eezeecommerce\AdminBundle\AdminEvents;
use eezeecommerce\AdminBundle\Event\AdminMenuEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AdminEventSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(
            AdminEvents::ADMIN_MENU_COMPLETED => array(
                array("onMenuComplete", 0)
            ),
        );
    }

    public function onMenuComplete(AdminMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu["settings"]["children"][] =
            array(
                "name" => "Payments",
                "icon" => "icon-",
                "link" => "_eezeecommerce_settings_payments",
                "children" => null
            );
        $event->setMenu($menu);
    }
}