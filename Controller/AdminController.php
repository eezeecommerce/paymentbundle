<?php

namespace eezeecommerce\PaymentBundle\Controller;

use eezeecommerce\PaymentBundle\Entity\GatewayConfig;
use eezeecommerce\PaymentBundle\Form\Type\Admin\CardNetType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\GatewayConfigType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\KlarnaInvoiceType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\OfflineType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\AuthorisenetType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\PayPalExpressType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\PayPalProType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\StripeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    public function indexAction(Request $request)
    {
        $setting = $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findAll();

        return $this->render("eezeecommercePaymentBundle::payments.html.twig", array(
            "settings" => $setting
        ));
    }

    public function paypalExpressAction(Request $request)
    {
        $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "paypal_express_checkout"));

        if (null === $setting) {
            $setting = new GatewayConfig();
            $setting->setGatewayName("eezee_paypal_express");
            $setting->setFactoryName("paypal_express_checkout");
        }

        $config = $setting->getConfig();
        $mergedConfig = array_merge($config, array("factory" => "paypal_express_checkout"));
        $setting->setConfig($mergedConfig);

        $form = $this->createForm(new PayPalExpressType(), $setting);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $config = $setting->getConfig();
            $config["sandbox"] ? $config["sandbox"] = true : $config["sandbox"] = false;
            $setting->setConfig($config);
            $em->persist($setting);
            $em->flush();
            return $this->redirectToRoute("_eezeecommerce_settings_payments");
        }

        return $this->render(
            "eezeecommercePaymentBundle::paypal_express_admin.html.twig",
            array(
                "form" => $form->createView()
            )
        );
    }

    public function klarnaInvoiceAction(Request $request)
    {
        $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "klarna_invoice"));

        if (null === $setting) {
            $setting = new GatewayConfig();
            $setting->setGatewayName("eezee_klarna_invoice");
            $setting->setFactoryName("klarna_invoice");
        }

        $config = $setting->getConfig();
        $mergedConfig = array_merge($config, array("factory" => "klarna_invoice"));
        $setting->setConfig($mergedConfig);

        $form = $this->createForm(new KlarnaInvoiceType(), $setting);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $config = $setting->getConfig();
            $config["sandbox"] ? $config["sandbox"] = true : $config["sandbox"] = false;
            $setting->setConfig($config);
            $em->persist($setting);
            $em->flush();
            return $this->redirectToRoute("_eezeecommerce_settings_payments");
        }

        return $this->render(
            "eezeecommercePaymentBundle::klarna_invoice_admin.html.twig",
            array(
                "form" => $form->createView()
            )
        );
    }

    public function stripeAction(Request $request)
    {
        $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "stripe_checkout"));

        if (null === $setting) {
            $setting = new GatewayConfig();
            $setting->setGatewayName("eezee_stripe_checkout");
            $setting->setFactoryName("stripe_checkout");
        }

        $config = $setting->getConfig();
        $mergedConfig = array_merge($config, array("factory" => "stripe_checkout"));
        $setting->setConfig($mergedConfig);

        $form = $this->createForm(new StripeType(), $setting);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $config = $setting->getConfig();
            $config["sandbox"] ? $config["sandbox"] = true : $config["sandbox"] = false;
            $setting->setConfig($config);
            $em->persist($setting);
            $em->flush();
            return $this->redirectToRoute("_eezeecommerce_settings_payments");
        }

        return $this->render(
            "eezeecommercePaymentBundle::stripe_admin.html.twig",
            array(
                "form" => $form->createView()
            )
        );
    }

    public function authoriseNetAction(Request $request)
    {
        $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "authorize_net_aim"));

        if (null === $setting) {
            $setting = new GatewayConfig();
            $setting->setGatewayName("eezee_authorisenet_checkout");
            $setting->setFactoryName("authorize_net_aim");
        }

        $config = $setting->getConfig();
        $mergedConfig = array_merge($config, array("factory" => "authorize_net_aim"));
        $setting->setConfig($mergedConfig);

        $form = $this->createForm(new AuthorisenetType(), $setting);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $config = $setting->getConfig();
            $config["sandbox"] ? $config["sandbox"] = true : $config["sandbox"] = false;
            $setting->setConfig($config);
            $em->persist($setting);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_settings_payments");
        }

        return $this->render(
            "eezeecommercePaymentBundle::authorisenet_admin.html.twig",
            array(
                "form" => $form->createView(),
            )
        );
    }

    public function worldpayAction(Request $request)
    {
        $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "omnipay_worldpay"));

        if (null === $setting) {
            $setting = new GatewayConfig();
            $setting->setGatewayName("eezee_omnipay_worldpay");
            $setting->setFactoryName("omnipay_worldpay");
        }

        $config = $setting->getConfig();
        $mergedConfig = array_merge($config, array("factory" => "omnipay_worldpay"));
        $setting->setConfig($mergedConfig);

        $form = $this->createForm(new StripeType(), $setting);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $config = $setting->getConfig();
            $config["sandbox"] ? $config["sandbox"] = true : $config["sandbox"] = false;
            $setting->setConfig($config);
            $em->persist($setting);
            $em->flush();
            return $this->redirectToRoute("_eezeecommerce_settings_payments");
        }

        return $this->render(
            "eezeecommercePaymentBundle::stripe_admin.html.twig",
            array(
                "form" => $form->createView()
            )
        );
    }

    public function cardNetAction(Request $request)
    {
        $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "cardnethosted"));

        if (null === $setting) {
            $setting = new GatewayConfig();
            $setting->setGatewayName("eezee_cardnet");
            $setting->setFactoryName("cardnethosted");
        }

        $config = $setting->getConfig();
        $mergedConfig = array_merge($config, array("factory" => "cardnethosted"));
        $setting->setConfig($mergedConfig);

        $form = $this->createForm(new CardNetType(), $setting);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $config = $setting->getConfig();
            $config["sandbox"] ? $config["sandbox"] = true : $config["sandbox"] = false;
            $setting->setConfig($config);
            $em->persist($setting);
            $em->flush();
            return $this->redirectToRoute("_eezeecommerce_settings_payments");
        }

        return $this->render(
            "eezeecommercePaymentBundle::card_net_admin.html.twig",
            array(
                "form" => $form->createView()
            )
        );
    }

    public function paypalProAction(Request $request)
    {
        $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "paypal_pro_checkout"));

        if (null === $setting) {
            $setting = new GatewayConfig();
            $setting->setGatewayName("eezee_paypal_pro");
            $setting->setFactoryName("paypal_pro_checkout");
        }

        $config = $setting->getConfig();
        $mergedConfig = array_merge($config, array("factory" => "paypal_pro_checkout","trxtype" => "S", "tender" => "C"));
        $setting->setConfig($mergedConfig);

        $form = $this->createForm(new PayPalProType(), $setting);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $config = $setting->getConfig();
            $config["sandbox"] ? $config["sandbox"] = true : $config["sandbox"] = false;
            $setting->setConfig($config);
            $em->persist($setting);
            $em->flush();
            return $this->redirectToRoute("_eezeecommerce_settings_payments");
        }

        return $this->render(
            "eezeecommercePaymentBundle::paypal_pro_admin.html.twig",
            array(
                "form" => $form->createView()
            )
        );
    }

    public function offlineAction(Request $request)
    {
        $setting = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "offline"));

        if (null === $setting) {
            $setting = new GatewayConfig();
            $setting->setGatewayName("offline");
            $setting->setFactoryName("offline");
        }
        $config = $setting->getConfig();
        $mergedConfig = array_merge($config, array("factory" => "offline"));
        $setting->setConfig($mergedConfig);

        $form = $this->createForm(new OfflineType(), $setting);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($setting);
            $em->flush();
            return $this->redirectToRoute("_eezeecommerce_settings_payments");
        }

        return $this->render(
            "eezeecommercePaymentBundle::offline_admin.html.twig",
            array(
                "form" => $form->createView()
            )
        );
    }
}