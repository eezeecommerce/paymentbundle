<?php

namespace eezeecommerce\PaymentBundle\Controller;

use eezeecommerce\DiscountBundle\Entity\DiscountCodes;
use eezeecommerce\OrderBundle\Entity\OptionsOrderlines;
use eezeecommerce\OrderBundle\Entity\OrderLines;
use Payum\Offline\Constants;
use Presta\SitemapBundle\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use eezeecommerce\OrderBundle\Entity\Orders;
use Payum\Core\Security\SensitiveValue;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Description of PaymentController
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
class PaymentController extends Controller
{

    public function prepareAction(Request $request)
    {

        $session = $request->getSession();

        $order = $this->get("eezeecommerce.order_manager");
        $cart = $order->getCart();

        if (!$order->hasOrder()) {
            $this->addFlash("warning", "Unfortunately, your session has expired. Please try again");
            return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
        }
        
        if ($cart->isEmpty()) {
            $this->addFlash("warning", "It seems that your cart is empty, please add items to your cart before proceeding");
            return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
        }

        $orderClass = $order->getOrder()->getEntity();

        $session->set("courier-options", $request->request->get("courier-options"));

        $shippingId = $session->get("courier-options");
        
        if (null === $shippingId) {
            $this->addFlash("warning", "No Shipping options have been set, please select a shipping option and try again.");
            return $this->redirect($this->generateUrl("_eezeecommerce_checkout"));
        }

        $shipping = $this->getDoctrine()->getRepository("eezeecommerceShippingBundle:CourierServicePricing")->find($shippingId);

        $weight = $cart->getWeight();

        if ($shippingId == "FREE" && $shipping === null) {
            $carriage = array(
                "price" => 0.00,
                "courierService" => array(
                    "name" => "Free Shipping",
                    "courier" => array(
                        "name" => "Free Shipping"
                    ),
                ),
            );
        }else {
            $carriage = $this->get("eezeecommerce_shipping.price.manager")->getPrice($shipping, $weight);
        }

        if ($request->request->has("min-del") && $request->request->has("max-del")) {
            $orderClass->setEstDelivery($request->request->get("min-del")." - ".$request->request->get("max-del"));
            $orderClass->setCourierDeliveryMin($request->request->get("courier-min-del"));
            $orderClass->setCourierDeliveryMax($request->request->get("courier-max-del"));
        }

        if ($request->request->has("notes")) {
            $orderClass->setNotes($request->request->get("notes"));
        }

        $orderClass->setShippingTotal($carriage["price"]);
        $orderClass->setCourierName($carriage['courierService']['courier']['name']);
        $orderClass->setCourierService($carriage['courierService']['name']);
        $orderClass->setSubtotal($cart->total());
        $total = number_format(($cart->total() - $cart->getDiscount()) + $orderClass->getShippingTotal(), 2 ,".", "");

        $orderClass->setTotal($total);

        $orderProcess = $this->get("eezeecommerce.order.processor");
        $orderProcess->setOrder($orderClass);
        $orderProcess->setUser($this->getUser());
        $tmpOrder = $orderProcess->prepareOrder();

        $orderClass = $this->getDoctrine()->getRepository(Orders::class)
            ->find($tmpOrder->getId());

        $orderProcess->process($orderClass);

        $orderClass = $this->getDoctrine()->getRepository(Orders::class)
            ->find($tmpOrder->getId());
        $order->setOrder($orderClass);

        $form = $request->request->get("eezeecommerce_payment_methods");

        $gatewayName = $form['gateway_name'];

        if (empty($gatewayName)) {
            $this->addFlash("warning", "Cannot process payment at this time. Please try again or contact sales");
            return $this->redirect($this->generateUrl("_eezeecommerce_checkout"));
        }

        $storage = $this->get("payum")->getStorage("eezeecommerce\PaymentBundle\Entity\PaymentDetails");

        $payment = $storage->create();
        $payment['gateway'] = $gatewayName;

        if ($gatewayName == "eezee_paypal_pro") {
            $payment['CURRENCY']  = $cart->getCurrencyCode();
            $payment['AMT'] = number_format($orderClass->getTotal(),2,".","");
            $payment["ACCT"] = new SensitiveValue($form["card"]["acct"]);
            $payment["CVV2"] = new SensitiveValue($form["card"]["cvv2"]);
            $payment["EXPDATE"] = new SensitiveValue(str_pad($form["card"]["exp_year"]["month"], 2, "0", STR_PAD_LEFT) . substr($form["card"]["exp_year"]["year"], -2));
            $payment["BILLTOFIRSTNAME"] = $orderClass->getBillingAddress()->getNameFirst();
            $payment["BILLTOLASTNAME"] = $orderClass->getBillingAddress()->getNameLast();
            $payment->setOrder($orderClass);

            $storage->update($payment);

            $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
                $gatewayName, $payment, '_eezeecommerce_payment_done'
            );

            return $this->forward('PayumBundle:Capture:do', array('payum_token' => $captureToken));
        }
        elseif ($gatewayName == "eezee_paypal_express") {

            $payment['PAYMENTREQUEST_0_CURRENCYCODE']  = $cart->getCurrencyCode();
            $payment['PAYMENTREQUEST_0_AMT'] = number_format($orderClass->getTotal(),2,".","");
            $payment->setOrder($orderClass);
            $storage->update($payment);

            $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
                    $gatewayName, $payment, '_eezeecommerce_payment_done'
            );
            return $this->redirect($captureToken->getTargetUrl());
        } elseif ($gatewayName == "offline") {
            $payment['currency']  = $cart->getCurrencyCode();
            $payment['amount'] = number_format($orderClass->getTotal(),2,".","");
            $payment[Constants::FIELD_PAID] = Constants::FIELD_PAID;
            $payment->setOrder($orderClass);
            $storage->update($payment);

            $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
                $gatewayName, $payment, '_eezeecommerce_payment_done'
            );
            return $this->redirect($captureToken->getTargetUrl());
        } elseif ($gatewayName == "eezee_cardnet") {
            $payment['currency']  = 826;
            $payment['chargetotal'] = number_format($orderClass->getTotal(),2,".","");
            $payment['responseSuccessURL'] = $this->generateUrl("_eezeecommerce_payment_cardnet_done", array(), UrlGeneratorInterface::ABSOLUTE_URL);
            $payment['responseFailURL'] = $this->generateUrl("_eezeecommerce_payment_cardnet_done", array(), UrlGeneratorInterface::ABSOLUTE_URL);

            $payment->setOrder($orderClass);
            $storage->update($payment);

            $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
                $gatewayName, $payment, '_eezeecommerce_payment_cardnet_done'
            );
            return $this->redirect($captureToken->getTargetUrl());
        } else {
            try {
                $payment['currency']  = $cart->getCurrencyCode();
                $payment['amount'] = number_format($orderClass->getTotal(),2,".","");
                $payment[Constants::FIELD_PAID] = Constants::FIELD_PAID;
                $payment->setOrder($orderClass);
                $storage->update($payment);

                $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
                    $gatewayName, $payment, '_eezeecommerce_payment_done'
                );
                return $this->redirect($captureToken->getTargetUrl());
            }   catch(Exception $e) {
                $this->addFlash("warning", "Cannot process payment at this time. Please try again or contact sales");
                return $this->redirect($this->generateUrl("_eezeecommerce_checkout"));
            }
        }
    }

}
