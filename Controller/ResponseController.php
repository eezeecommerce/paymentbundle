<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\PaymentBundle\Controller;

use eezeecommerce\OrderBundle\Entity\OrderLines;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Payum\Core\Request\GetHumanStatus;
use Symfony\Component\HttpFoundation\JsonResponse;
use eezeecommerce\StockBundle\StockEvents;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of ResponseController
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
class ResponseController extends Controller
{

    public function doneAction(Request $request)
    {
        $order = $this->get("eezeecommerce.order_manager");
        $cart = $this->get("eezeecommerce.cart");

        if ($cart->isEmpty() || $order->isEmpty()) {
            $this->addFlash("warning", "Unfortunately, your session has expired. Please try again");
            return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
        }

        $token = $this->get('payum')->getHttpRequestVerifier()->verify($request);

        $identity = $token->getDetails();
        $this->get("payum")->getStorage($identity->getClass())->find($identity);

        $gateway = $this->get('payum')->getGateway($token->getGatewayName());

        $this->get('payum')->getHttpRequestVerifier()->invalidate($token);

        $gateway->execute($status = new GetHumanStatus($token));

        $payment = $status->getFirstModel();

        $session = $request->getSession();

        $em = $this->getDoctrine()->getManager();

        $payment->setStatus($status->getValue());

        $em->persist($payment);
        $em->flush();

        $orderClass = $order->getOrder()->getEntity();

        $orderNumber = $orderClass->getId();

        if ($status->getValue() != "success" && $status->getValue() != "captured") {
            $total = number_format((($orderClass->getSubtotal() - $orderClass->getTaxExemptTotal() - $orderClass->getDiscountTotal())), 2 ,".", "");
            
            $orderClass->setTotal($total);

            $order->setOrder($orderClass);
            $this->addFlash(
                    "danger", "Unfortunately, your payment has failed. Please try again or contact customer support."
            );
            return $this->redirectToRoute("_eezeecommerce_checkout");
        }

        $stockManager = $this->get("eezeecommerce.stock_bundle.manager");

        $orderEntity = $this->getDoctrine()->getRepository("eezeecommerceOrderBundle:Orders")
            ->find($orderClass->getId());

        foreach ($orderEntity->getOrderLines() as $orderLines) {
            if (!$orderLines instanceof OrderLines) {
                throw new \InvalidArgumentException("Orderline is not instance of OrderLines Entity");
            }

            $stock = $orderLines->getProduct()->getStock();


            if (null !== ($variant = $orderLines->getVariant())) {
                $stock = $variant->getStock();
            }

            $stockManager->setProduct($stock, ($orderLines->getQty() * -1));
            $stockManager->persist();

        }

        $orderNumber = ($orderNumber + 10000);

        $orderEntity->setOrderNumber($orderNumber);

        $em->persist($orderEntity);
        $em->flush();

        $settings = $this->get("eezeecommerce_settings.setting");

        $to = $orderClass->getBillingAddress()->getContactEmail();
        $params = array(
            "orders" => $orderEntity,
            "settings" => $settings->loadSettingsBySite(),
        );
        $locale = $request->getLocale();

        $message = $this->get('lexik_mailer.message_factory')->get('order-success', $to, $params, $locale);

        $this->get('mailer')->send($message);


        $session->clear();

        $session->set("order_success", $orderNumber);
        $session->save();


        return $this->redirect($this->generateUrl("_eezeecommerce_payment_success"));
    }


    public function orderSuccessAction(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has("order_success")) {
            throw $this->createNotFoundException();
        }

        $orderNumber = $session->get("order_success");

        $order = $this->getDoctrine()->getRepository("eezeecommerceOrderBundle:Orders")
            ->findOneBy(array("order_number" => $orderNumber));

        if (count($order) < 1) {
            throw $this->createNotFoundException();
        }


        return $this->render("AppBundle:Frontend/Checkout:success.html.twig", array("ordernumber" => $orderNumber, "order" => $order));
    }


    public function orderCardNetSuccessAction(Request $request)
    {
        $order = $this->get("eezeecommerce.order_manager");
        $cart = $this->get("eezeecommerce.cart");
        $session = $request->getSession();

        if ($cart->isEmpty() || $order->isEmpty()) {
            $this->addFlash("warning", "Unfortunately, your session has expired. Please try again");
            return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
        }

        if (!$request->request->has("status")) {
            $this->addFlash(
                "danger", "Unfortunately, your payment has failed. Please try again or contact customer support."
            );
            return $this->redirectToRoute("_eezeecommerce_checkout");
        }

        $status = $request->request->get("status");

        $em = $this->getDoctrine()->getManager();

        $orderClass = $order->getOrder()->getEntity();

        $orderNumber = $orderClass->getId();

        if ($status != "success" && $status != "captured" && $status != "APPROVED") {
            $total = number_format((($orderClass->getSubtotal() - $orderClass->getTaxExemptTotal() - $orderClass->getDiscountTotal())), 2 ,".", "");

            $orderClass->setTotal($total);


            $order->setOrder($orderClass);
            $this->addFlash(
                "danger", "Unfortunately, your payment has failed. Please try again or contact customer support."
            );
            return $this->redirectToRoute("_eezeecommerce_checkout");
        }

        $stockManager = $this->get("eezeecommerce.stock_bundle.manager");

        $orderEntity = $this->getDoctrine()->getRepository("eezeecommerceOrderBundle:Orders")
            ->find($orderClass->getId());

        foreach ($orderEntity->getOrderLines() as $orderLines) {
            if (!$orderLines instanceof OrderLines) {
                throw new \InvalidArgumentException("Orderline is not instance of OrderLines Entity");
            }

            $stock = $orderLines->getProduct()->getStock();


            if (null !== ($variant = $orderLines->getVariant())) {
                $stock = $variant->getStock();
            }

            $stockManager->setProduct($stock, ($orderLines->getQty() * -1));
            $stockManager->persist();

        }

        $orderNumber = ($orderNumber + 10000);

        $orderEntity->setOrderNumber($orderNumber);

        $em->persist($orderEntity);
        $em->flush();

        $settings = $this->get("eezeecommerce_settings.setting");

        $to = $orderClass->getBillingAddress()->getContactEmail();
        $params = array(
            "orders" => $orderEntity,
            "settings" => $settings->loadSettingsBySite(),
        );
        $locale = $request->getLocale();

        $message = $this->get('lexik_mailer.message_factory')->get('order-success', $to, $params, $locale);

        $this->get('mailer')->send($message);


        $session->clear();

        $session->set("order_success", $orderNumber);
        $order = $em->getRepository("eezeecommerceOrderBundle:Orders")->find(($orderNumber - 10000));
        $payment = $em->getRepository("eezeecommercePaymentBundle:PaymentDetails")->findOneByOrder($order);
        $payment->setStatus("captured");
        $em->persist($payment);
        $em->flush();
        $session->save();


        return $this->redirect($this->generateUrl("_eezeecommerce_payment_success"));
    }

}
