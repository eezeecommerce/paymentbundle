<?php

namespace eezeecommerce\PaymentBundle\Form\Type\Admin;

use eezeecommerce\PaymentBundle\Form\Type\Admin\Extension\WorldpayConfigType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class StripeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("factoryName", HiddenType::class)
            ->add("gatewayName", null, array(
                "disabled" => true
            ))
            ->add("name")
            ->add("enabled", CheckboxType::class, array(
                "required" => false
            ))
            ->add("config", new WorldpayConfigType())
            ->add("Submit", SubmitType::class);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\PaymentBundle\Entity\GatewayConfig'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_paymentbundle_gatewayconfig';
    }
}
