<?php

namespace eezeecommerce\PaymentBundle\Form\Type\Admin\Extension;

use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorldpayConfigType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("installationId")
            ->add("accountId")
            ->add("secretWord")
            ->add("callbackPassword")
            ->add("testMode", ChoiceType::class, array(
                "choices" => array(
                    false => "Disabled",
                    true => "Enabled"
                )
            ))->add("factory", HiddenType::class, array(
                "disabled" => true
            ))->add("gateway_name", HiddenType::class, array(
                "disabled" => true
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_paymentbundle_klarna_invoice_extension';
    }
}