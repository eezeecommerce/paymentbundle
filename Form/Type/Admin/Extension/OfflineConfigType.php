<?php

namespace eezeecommerce\PaymentBundle\Form\Type\Admin\Extension;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class OfflineConfigType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("factory", HiddenType::class, array(
            "disabled" => true
        ))->add("gateway_name", HiddenType::class, array(
            "disabled" => true
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_paymentbundle_offline_extension';
    }
}