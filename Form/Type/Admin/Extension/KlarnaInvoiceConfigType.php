<?php

namespace eezeecommerce\PaymentBundle\Form\Type\Admin\Extension;

use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KlarnaInvoiceConfigType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("eid")
            ->add("secret")
            ->add("country", ChoiceType::class, array(
                "choices" => array(
                    "AT" => "Austria",
                    "DK" => "Denmark",
                    "FI" => "Finland",
                    "DE" => "Germany",
                    "NL" => "Netherlands",
                    "NO" => "Norway",
                    "SE" => "Sweden"
                )
            ))
            ->add("language", ChoiceType::class, array(
                "choices" => array(
                    "DA" => "Danish",
                    "DE" => "German",
                    "EN" => "English",
                    "FI" => "Finish",
                    "NB" => "Norwegian",
                    "NL" => "Dutch",
                    "SV" => "Swedish"
                )
            ))
            ->add("currency", ChoiceType::class, array(
                "choices" => array(
                    "SEK" => "Swedish Krona",
                    "NOK" => "Norwegian Krone",
                    "EUR" => "Euros",
                    "DKK" => "Danish Krone"
                )
            ))
            ->add("sandbox", ChoiceType::class, array(
                "choices" => array(
                    false => "Disabled",
                    true => "Enabled"
                )
            ))->add("factory", HiddenType::class, array(
                "disabled" => true
            ))->add("gateway_name", HiddenType::class, array(
                "disabled" => true
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_paymentbundle_klarna_invoice_extension';
    }
}