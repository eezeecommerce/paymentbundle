<?php

namespace eezeecommerce\PaymentBundle\Form\Type\Admin;

use eezeecommerce\PaymentBundle\Form\Type\Admin\Extension\OfflineType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\Extension\PaypalExpressType;
use eezeecommerce\PaymentBundle\Form\Type\Admin\GatewayConfigType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentSettingsType extends AbstractType
{
    /**
     * Form Builder
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sandbox');
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver Resolver instance
     *
     * @inheritdoc
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\PaymentBundle\Entity\PaymentSettings'
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_paymentbundle_paymentsettings';
    }
}
