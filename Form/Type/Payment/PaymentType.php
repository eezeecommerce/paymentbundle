<?php

namespace eezeecommerce\PaymentBundle\Form\Type\Payment;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("gateway_name", EntityType::class, array(
                'class' => "eezeecommercePaymentBundle:GatewayConfig",
                'query_builder' => function(EntityRepository $er) {
                    $r = $er->createQueryBuilder('g')
                        ->where("g.enabled = 1")
                        ->orderBy("g.name", "ASC");
                    return $r;

                },
                'choice_label' => "name",
                'choice_value' => "gateway_name",
                "label"=> false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_payment_methods';
    }
}