<?php

namespace eezeecommerce\PaymentBundle\Form\Type\Payment;

use Payum\Core\Bridge\Symfony\Form\Type\CreditCardExpirationDateType;
use Symfony\Component\Form\AbstractType;
use eezeecommerce\PaymentBundle\Entity\PaymentDetails;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaypalProType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("gateway_name", HiddenType::class)
            ->add(
                'acct', null, [
                    "required" => false,
                    "label" => false,
                ]
            )
            ->add(
                'exp_year', CreditCardExpirationDateType::class, [
                    "label" => false,
                ]
            )
            ->add(
                'cvv2', null, [
                    "required" => false,
                    "label" => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => PaymentDetails::class
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_payment_paypal_pro';
    }
}